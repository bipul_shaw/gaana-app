export const artistAction = (artist) => {
    return {
        type: "Artist",
        payload: artist
    }
}