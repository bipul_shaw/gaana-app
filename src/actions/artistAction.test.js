import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { artistAction } from './artistAction';
Enzyme.configure({ adapter: new Adapter() });

describe('Artist actions', () => {
  it('should create an action to call reducer', () => {
    const text = 'Finish docs';
    const expectedAction = {
      type: 'Artist',
      payload: text
    };
    expect(artistAction(text)).toEqual(expectedAction);
  });
});
