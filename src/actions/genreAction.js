export const genreAction = (genre) => {
    return {
        type: "Genre",
        payload: genre
    }
}