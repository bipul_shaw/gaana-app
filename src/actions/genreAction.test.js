import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { sgenreAction } from './genreAction'
Enzyme.configure({ adapter: new Adapter() });

describe('actions', () => {
  it('should create an action to call reducer', () => {
    const text = 'Sad';
    const expectedAction = {
      type: 'Genre',
      payload: text
    };
    expect(genreAction(text)).toEqual(expectedAction);
  });
});
