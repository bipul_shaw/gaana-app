export const searchAction = (name) => {
    return {
        type: "Search",
        payload: name
    }
}