import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { searchAction } from './searchAction';
Enzyme.configure({ adapter: new Adapter() });

describe('This test', () => {
  it('should create an search action to call reducer', () => {
    const text = 'Chitra';
    const expectedAction = {
      type: 'Search',
      payload: text
    };
    expect(searchAction(text)).toEqual(expectedAction);
  });
});
