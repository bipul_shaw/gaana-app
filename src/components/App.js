import React, { Component } from 'react';
import MusicCard from './musicCard';
import SearchBar from './searchBar';
import { connect } from 'react-redux';
import GenreButton from './genreButton';
import dataModifier from '../store/dataModifier';
import ArtistButton from './artistButton';
import './App.css';

export class App extends Component {
  genreList = [
    "Hip-Hop",
    "Pop",
    "Rock",
    "Romantic",
    "Bollywood",
    "Classical",
    "Folk",
    "Jazz",
    "Sad",
    "Country"
  ];
  artistList = [
    "Chitra",
    "Navv Inder",
    "Ustad Rashid Khan",
    "Christiana Perri",
    "Vishal Dadlani",
    "Miles Davis",
    "Kailash Kher",
    "Celine Dion",
    "Queen",
    "Neha Bhasin"
  ];
  render() {
    return (
      <div className="backGround">
        <div className="filterComponent">
          <h3>Filter options</h3>
          <GenreButton label="RESET ALL" />
          <div className="filterOptions">
            <h3>Genres</h3>
            {this.genreList.map((genre, i) => (<GenreButton key={i} label={genre} selectedGenres={this.props.newData.allGenres} />))}
            <h3>Artists</h3>
            {this.artistList.map((artist, i) => (<ArtistButton key={i} label={artist} selectedArtists={this.props.newData.allArtists} />))}
          </div>
        </div>
        <div className="body">
          <div className="head">
            <div className="heading">gaana</div>
            <SearchBar value={this.props.newData.searchBoxText} />
          </div>
          <div className="songs">
            {this.props.newSongs.map((song, i) => (<MusicCard key={i} allData={song} />))}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    newData: state,
    newSongs: dataModifier(state)
  };
};

export default connect(mapStateToProps)(App);