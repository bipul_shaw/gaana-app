import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { App } from './App';
import reducer from '../store/reducer';
import dataModifier from '../store/dataModifier'
Enzyme.configure({ adapter: new Adapter() });

describe('App component', () => {
  const state = reducer(undefined, {});
  const wrapper = Enzyme.shallow(<App newData={state} newSongs={dataModifier(state)} />);
  it('should be rendered correctly', () => {
    expect(wrapper.find('.backGround')).toHaveLength(1);
  });
  it('should render filter component once', () => {
    expect(wrapper.find('.filterComponent')).toHaveLength(1);
  });
  it('should render Search component component once', () => {
    expect(wrapper.find('.heading')).toHaveLength(1);
  });
  it('should render songs component once', () => {
    expect(wrapper.find('.songs')).toHaveLength(1);
  });
});