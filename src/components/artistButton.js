import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { artistAction } from '../actions/artistAction';
import './App.css';
export class ArtistButton extends React.Component {
  render() {
    var cn;
    if (this.props.selectedArtists.includes(this.props.data))
      cn = "clickedButton";
    else
      cn = "filterButton";
    return (
      <button className={cn} onClick={() => this.props.artistAction(this.props.label)}>{this.props.label}</button>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ artistAction }, dispatch);
};

export default connect(null, mapDispatchToProps)(ArtistButton);