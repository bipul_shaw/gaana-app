import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { ArtistButton } from './artistButton';
Enzyme.configure({ adapter: new Adapter() });

describe("<ArtistButton> Component", () => {
  const artistMock = jest.fn();
  const initProps = "Chitra";
  const wrapper = Enzyme.shallow(<ArtistButton data={initProps} flag={[]} artistAction={artistMock} />);
  it('should be rendered correctly', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
  it('should call action function correctly',()=>{
    wrapper.find('button').simulate('click', initProps);
    expect(artistMock).toHaveBeenCalledWith('Chitra');
  });
})
