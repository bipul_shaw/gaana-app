import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { genreAction } from '../actions/genreAction';
import './App.css';
export class GenreButton extends React.Component {
  render() {
    var cn;
    if (this.props.label === "RESET ALL")
      cn = "reset";
    else if (this.props.selectedGenres.includes(this.props.label))
      cn = "clickedButton";
    else
      cn = "filterButton";
    return (
      <button className={cn} onClick={() => this.props.genreAction(this.props.label)}>{this.props.label}</button>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ genreAction }, dispatch);
};

export default connect(null, mapDispatchToProps)(GenreButton);