import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { GenreButton } from './genreButton';
Enzyme.configure({ adapter: new Adapter() });

describe("<GenreButton> Component", () => {
  const genreMock = jest.fn();
  const initProps = "Sad";
  const wrapper = Enzyme.shallow(<GenreButton label={initProps} genreAction={genreMock} />);
  it('should be rendered correctly', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
  it('should call action function correcty', () => {
    wrapper.find('button').simulate('click', initProps);
    expect(genreMock).toHaveBeenCalledWith('Sad');
  });
})
