import React from 'react';
import './App.css';
class MusicCard extends React.Component {
  render() {
    return (
      <div className="musicCard">
        <img src={this.props.allData.thumbnail} className="imag" alt="Cover pic" />
        <p className="description">Song: {this.props.allData.name}</p>
        <p className="description">Artist: {this.props.allData.artist}</p>
        <p className="description">Duration: {this.props.allData.duration}</p>
        <p className="description">Genre: {this.props.allData.genre}</p>
      </div>
    );
  }
}

export default MusicCard;