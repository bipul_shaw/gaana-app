import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import MusicCard from './musicCard';
Enzyme.configure({ adapter: new Adapter() });

describe('Music card test', () => {
  it('should render <MusicCard> component correctly', () => {
    const song = {
      name: "Kasam ki kasam",
      artist: "Chitra",
      duration: "7.05",
      genre: "Sad",
      thumbnail: "http://1.bp.blogspot.com/_1Wi6ZrOtVRY/RrayuOpQ02I/AAAAAAAAAF4/mnEMXzMzVv8/s320/kasam.JPG"
    }
    const wrapper = Enzyme.shallow(<MusicCard allData={song} />);
    expect(wrapper.debug()).toMatchSnapshot();
  });
});