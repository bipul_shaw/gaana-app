import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { searchAction } from '../actions/searchAction';
import si from './searchicon2.png';
import './App.css';
export class SearchBar extends React.Component {
  render() {
    return (
      <div className="both">
        <div className="searchIcon">
          <img src={si} className="imag2" alt="Search" />
        </div>
        <div className="searchDiv">
          <input type="text" placeholder="Search for songs" className="searchBar" value={this.props.value}
            onChange={(event) => this.props.searchAction(event.target.value)}/>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ searchAction }, dispatch);
};

export default connect(null, mapDispatchToProps)(SearchBar);