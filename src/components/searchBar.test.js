import React from 'react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { SearchBar } from './searchBar';
Enzyme.configure({ adapter: new Adapter() });

describe('This test', () => {
  const searchMock = jest.fn();
  const event = {
    target: {
      value: 'Search Key'
    }
  };
  const wrapper = Enzyme.shallow(<SearchBar value={event.target.value} searchAction={searchMock} />);
  it('should render <SearchBar/> component correctly', () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
  it('should call the action function correctly', ()=>{
    wrapper.find('.searchBar').simulate('change', event);
    expect(searchMock).toHaveBeenCalledWith('Search Key');
  });
});
