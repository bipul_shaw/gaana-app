import { createSelector } from 'reselect';

const songs = (state) => state.allSongs;
const artists = (state) => state.allArtists;
const genres = (state) => state.allGenres;
const text = (state) => state.searchBoxText.trim();

const modifier = (songList, artistFilters, genreFilters, searchedText) => {
  var newState = [];
  newState = songList.filter(song => (song.name.toLowerCase()).includes(searchedText) || (song.artist.toLowerCase()).includes(searchedText));
  if (artistFilters.length !== 0) {
    newState = newState.filter(song => artistFilters.includes(song.artist));
  }
  if (genreFilters.length !== 0) {
    newState = newState.filter(song => genreFilters.includes(song.genre));
  }
  newState.sort(function (a, b) {
    var nameA = a.name.toUpperCase();
    var nameB = b.name.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    else if (nameA > nameB) {
      return 1;
    }
    else {
      return 0;
    }
  });
  return newState;
}

export default createSelector(
  songs,
  artists,
  genres,
  text,
  modifier
);
