import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import dataModifier from './dataModifier';
import { testSongList } from './testSongList';
Enzyme.configure({ adapter: new Adapter() });

describe('dataModifier', () => {

  it('should return all the songs when there are no filters and no searched text', () => {
    let expectedData = testSongList;
    let state = { allSongs: testSongList, allArtists: [], allGenres: [], searchBoxText: '' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should not change the displayed songs if multiple space is given in search bar', () => {
    let expectedData = testSongList;
    let state = { allSongs: testSongList, allArtists: [], allGenres: [], searchBoxText: '      ' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should return no songs if any character other than alphabets is also searched in search box', () => {
    let expectedData = [];
    let state = { allSongs: testSongList, allArtists: [], allGenres: [], searchBoxText: 'sun0' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should return all the songs of a genre when it is only selected ', () => {
    let expectedData = [
      { name: "Aaoge jab tum o saajna", artist: "Ustad Rashid Khan", duration: "4.40", genre: "Classical", thumbnail: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4WPlkfE5jaffeilq3yH_zQY3JyVRuwAEtBpVkJoQIJoG3bdwqww" }
    ];
    let state = { allSongs: testSongList, allArtists: [], allGenres: ['Classical'], searchBoxText: '' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should return all the songs of a artist when it is only selected', () => {
    let expectedData = [
      { name: "A Thousand Years", artist: "Christiana Perri", duration: "5.07", genre: "Pop", thumbnail: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYg5OrIWAGFULKS6OtYLr3er1gM6F5aU3p6U6P66urimqUDdXd" }
    ];
    let state = { allSongs: testSongList, allArtists: ["Christiana Perri"], allGenres: [], searchBoxText: '' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should return all the songs which has the searched text in their name or artist name and when filtered only by search', () => {
    let expectedData = [
      { name: "What makes you beautiful", artist: "One Direction", genre: "Pop", duration: "3:18", thumbnail: "https://i.ytimg.com/vi/QJO3ROT-A4E/maxresdefault.jpg" }
    ];
    let state = { allSongs: testSongList, allArtists: [], allGenres: [], searchBoxText: 'what' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should return all the songs which has the selected genre as well as selected artist', () => {
    let expectedData = [
      { name: "Hello", artist: "Adele", genre: 'Pop', duration: "4:55", thumbnail: "https://img.cache.vevo.com/thumb/cms/e331b4d0-3a5b-11e7-8afe-c35019589c0d.jpg" }
    ];
    let state = { allSongs: testSongList, allArtists: ["Adele", "Ustad Rashid Khan"], allGenres: ["Pop"], searchBoxText: '' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should return all the songs which has searched text in its name or artist and is of that selected genre', () => {
    let expectedData = [
      { name: "Enchanted", artist: "Taylor Swift", genre: "Pop", duration: "5:53", thumbnail: 'https://img.buzzfeed.com/buzzfeed-static/static/2018-04/16/10/asset/buzzfeed-prod-web-04/sub-buzz-16652-1523887341-1.jpg?downsize=700:*&output-format=auto&output-quality=auto' }
    ];
    let state = { allSongs: testSongList, allArtists: [], allGenres: ["Pop"], searchBoxText: 'ench' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should return all the songs which has selected artist and searched text in its name or artist', () => {
    let expectedData = [
      { name: "A Thousand Years", artist: "Christiana Perri", duration: "5.07", genre: "Pop", thumbnail: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYg5OrIWAGFULKS6OtYLr3er1gM6F5aU3p6U6P66urimqUDdXd" }
    ];
    let state = { allSongs: testSongList, allArtists: ["Christiana Perri"], allGenres: [], searchBoxText: 'thou' };
    expect(dataModifier(state)).toEqual(expectedData);
  });

  it('should contain all the songs which are of selected artist, selected genre and searched text in its name and artist', () => {
    let expectedData = [
      { name: "Enchanted", artist: "Taylor Swift", genre: "Pop", duration: "5:53", thumbnail: 'https://img.buzzfeed.com/buzzfeed-static/static/2018-04/16/10/asset/buzzfeed-prod-web-04/sub-buzz-16652-1523887341-1.jpg?downsize=700:*&output-format=auto&output-quality=auto' }
    ];
    let state = { allSongs: testSongList, allArtists: ["Taylor Swift", "Adele"], allGenres: ["Pop", "Classical"], searchBoxText: 'encha' };
    expect(dataModifier(state)).toEqual(expectedData);
  });
});