import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import reducer, { songList } from './reducer';
Enzyme.configure({ adapter: new Adapter() });

describe('Reducer which gives directions to Reselect function via data', () => {
  let selectedArtists = [];
  let selectedGenres = [];
  let searchedText = '';
  let data = { allSongs: songList, allArtists: [], allGenres: [], searchBoxText: "" };
  it('should return the initial data at first', () => {
    let expectedData = data;
    let action = {};
    expect(reducer(undefined, action)).toEqual(expectedData);
  });
  it('should return original initial if Reset button is clicked', () => {
    let expectedData = data;
    let action = {
      type: "Genre",
      payload: 'RESET ALL'
    };
    expect(reducer(data, action)).toEqual(expectedData);
  });
  it('should return modified data for Reselect function if any genre filter button was clicked', () => {
    let expectedData = data;
    expectedData[2] = expectedData[2].concat("Sad");
    let action = {
      type: "Genre",
      payload: 'Sad'
    };
    expect(reducer(data, action)).toEqual(expectedData);
  });
  it('should return modified data for Reselect function if any artist filter button was clicked', () => {
    let expectedData = data;
    expectedData[1] = expectedData[1].concat("Chitra");
    let action = {
      type: "Artist",
      payload: 'Chitra'
    };
    expect(reducer(data, action)).toEqual(expectedData);
  });
  it('should return modified data for Reselect function if any text is written on search area', () => {
    let expectedData = data;
    expectedData[3] = 'kas0';
    let action = {
      type: "Search",
      payload: 'Kas0'
    };
    expect(reducer(data, action)).toEqual(expectedData);
  });
});