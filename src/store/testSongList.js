export const testSongList = [
    { name: "A Thousand Years", artist: "Christiana Perri", duration: "5.07", genre: "Pop", thumbnail: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSYg5OrIWAGFULKS6OtYLr3er1gM6F5aU3p6U6P66urimqUDdXd" },
    { name: "Aaoge jab tum o saajna", artist: "Ustad Rashid Khan", duration: "4.40", genre: "Classical", thumbnail: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4WPlkfE5jaffeilq3yH_zQY3JyVRuwAEtBpVkJoQIJoG3bdwqww" },
    { name: "Enchanted", artist: "Taylor Swift", genre: "Pop", duration: "5:53", thumbnail: 'https://img.buzzfeed.com/buzzfeed-static/static/2018-04/16/10/asset/buzzfeed-prod-web-04/sub-buzz-16652-1523887341-1.jpg?downsize=700:*&output-format=auto&output-quality=auto' },
    { name: "Hello", artist: "Adele", genre: 'Pop', duration: "4:55", thumbnail: "https://img.cache.vevo.com/thumb/cms/e331b4d0-3a5b-11e7-8afe-c35019589c0d.jpg" },
    { name: "What makes you beautiful", artist: "One Direction", genre: "Pop", duration: "3:18", thumbnail: "https://i.ytimg.com/vi/QJO3ROT-A4E/maxresdefault.jpg" }
];